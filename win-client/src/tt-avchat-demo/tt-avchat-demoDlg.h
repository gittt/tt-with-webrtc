
// tt-avchat-demoDlg.h : 头文件
//

#pragma once

#include "AVChatTT.h"
#include "CallFactory.h"
#include "afxwin.h"

#include "VideoRendererForWindows.h"
#include <memory>

// CttavchatdemoDlg 对话框
class CttavchatdemoDlg : public CDialogEx , public CallEventObserver, public VideoRendererObserver
{
// 构造
public:
	CttavchatdemoDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_TTAVCHATDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonCreateCall();
	afx_msg void OnBnClickedButtonAnswerCall();

	virtual void OnOfferSDP(const char * offer, int len) override;

	virtual void OnAnswerSDP(const char *offer, int len) override;

	virtual void OnFailure(ErrorMessageType type, const char *message) override;

	afx_msg void OnBnClickedButtonHangup();

	afx_msg void OnBnClickedButtonLoopback();
	
	afx_msg void OnClose();

	CStatic mVideoRenderer;

	/*视频渲染*/
	std::auto_ptr<VideoRendererForWindows> local_renderer_;
	std::auto_ptr<VideoRendererForWindows> remote_renderer_;
	void DrawVideoFrame(HWND wnd);
	HDRAWDIB ddh_;
	virtual void OnVideoFrame() override;

	CStatic mStatusLabel;
};
