/*
Navicat MySQL Data Transfer

Source Server         : teamtalk
Source Server Version : 50541
Source Host           : 172.16.211.60:3306
Source Database       : teamtalk

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2015-08-12 20:26:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for IMAccess
-- ----------------------------
DROP TABLE IF EXISTS `IMAccess`;
CREATE TABLE `IMAccess` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAccess
-- ----------------------------
INSERT INTO `IMAccess` VALUES ('2', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('2', '84', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('2', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('3', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('1', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('3', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('4', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('4', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '3', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '4', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '5', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '6', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '11', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('5', '25', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('5', '51', '2', '25', null);
INSERT INTO `IMAccess` VALUES ('1', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('1', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '114', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '90', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '84', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('3', '69', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '37', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '36', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '35', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '34', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '33', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '32', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '31', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '32', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '31', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '203', '3', '202', null);
INSERT INTO `IMAccess` VALUES ('1', '113', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '112', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '99', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '111', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '90', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '114', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '202', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '6', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('1', '50', '3', '40', null);

-- ----------------------------
-- Table structure for IMAdmin
-- ----------------------------
DROP TABLE IF EXISTS `IMAdmin`;
CREATE TABLE `IMAdmin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `bind_account` varchar(50) NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAdmin
-- ----------------------------
INSERT INTO `IMAdmin` VALUES ('1', 'admin', '管理员', 'd743e5e5860400b776e2f74a03049f40', '', '1439374476', '172.16.1.252', '1485', '8888', 'luxingzhan@sina.com', '备注信息', '1222907803', '1359281644', '1', '0', '');
INSERT INTO `IMAdmin` VALUES ('2', 'demo', '演示', 'fe01ce2a7fbac8fafaed7c982a04e229', '', '1375172281', '127.0.0.1', '103', '8888', '', '', '1239783735', '1254325770', '1', '0', '');

-- ----------------------------
-- Table structure for IMAudio
-- ----------------------------
DROP TABLE IF EXISTS `IMAudio`;
CREATE TABLE `IMAudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromId` int(11) unsigned NOT NULL COMMENT '发送者Id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收者Id',
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '语音存储的地址',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `duration` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '语音时长',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_fromId_toId` (`fromId`,`toId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMAudio
-- ----------------------------

-- ----------------------------
-- Table structure for IMAuthGroup
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthGroup`;
CREATE TABLE `IMAuthGroup` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthGroup
-- ----------------------------
INSERT INTO `IMAuthGroup` VALUES ('6', '旗舰店', '');
INSERT INTO `IMAuthGroup` VALUES ('7', '出版社', '');
INSERT INTO `IMAuthGroup` VALUES ('8', '图书管理员', '7,8,9,10,11,12');

-- ----------------------------
-- Table structure for IMAuthGroupAccess
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthGroupAccess`;
CREATE TABLE `IMAuthGroupAccess` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_2` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthGroupAccess
-- ----------------------------
INSERT INTO `IMAuthGroupAccess` VALUES ('1', '7');
INSERT INTO `IMAuthGroupAccess` VALUES ('2', '6');
INSERT INTO `IMAuthGroupAccess` VALUES ('3', '6');

-- ----------------------------
-- Table structure for IMAuthRule
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthRule`;
CREATE TABLE `IMAuthRule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '',
  `title` char(40) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthRule
-- ----------------------------
INSERT INTO `IMAuthRule` VALUES ('6', 'book_upload', '图书上传权限', '0', '');
INSERT INTO `IMAuthRule` VALUES ('7', 'edit_any', '可编辑他人数据', '0', '');
INSERT INTO `IMAuthRule` VALUES ('8', 'admin_order', '管理用户订单', '0', '');
INSERT INTO `IMAuthRule` VALUES ('9', 'admin_book', '管理图书', '0', '');
INSERT INTO `IMAuthRule` VALUES ('10', 'edit_shop', '编辑店铺信息', '0', '');
INSERT INTO `IMAuthRule` VALUES ('11', 'beihuo', '备货权限', '0', '');
INSERT INTO `IMAuthRule` VALUES ('12', 'admin_beihuo', '管理备货订单', '0', '');

-- ----------------------------
-- Table structure for IMDepart
-- ----------------------------
DROP TABLE IF EXISTS `IMDepart`;
CREATE TABLE `IMDepart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `departName` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '部门名称',
  `priority` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '显示优先级',
  `parentId` int(11) unsigned NOT NULL COMMENT '上级部门id',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_departName` (`departName`),
  KEY `idx_priority_status` (`priority`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMDepart
-- ----------------------------
INSERT INTO `IMDepart` VALUES ('1', '默认注册部门', '0', '0', '0', '1439370664', '1439370664');

-- ----------------------------
-- Table structure for IMDiscovery
-- ----------------------------
DROP TABLE IF EXISTS `IMDiscovery`;
CREATE TABLE `IMDiscovery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `itemName` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '名称',
  `itemUrl` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'URL',
  `itemPriority` int(11) unsigned NOT NULL COMMENT '显示优先级',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_itemName` (`itemName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMDiscovery
-- ----------------------------
INSERT INTO `IMDiscovery` VALUES ('1', 'ceshi1', 'http://www.baidu.com', '0', '1', '1438520296', '0');
INSERT INTO `IMDiscovery` VALUES ('2', 'sssss', 'http://www.baidu.com', '0', '0', '0', '0');
INSERT INTO `IMDiscovery` VALUES ('3', 'ccccc', 'http://www.baidu.com', '0', '0', '0', '0');
INSERT INTO `IMDiscovery` VALUES ('4', 'bbbb', 'http://www.baidu.com', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for IMGroup
-- ----------------------------
DROP TABLE IF EXISTS `IMGroup`;
CREATE TABLE `IMGroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '群名称',
  `avatar` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '群头像',
  `creator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者用户id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '群组类型，1-固定;2-临时群',
  `userCnt` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成员人数',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否删除,0-正常，1-删除',
  `version` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '群版本号',
  `lastChated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后聊天时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`(191)),
  KEY `idx_creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群信息';

-- ----------------------------
-- Records of IMGroup
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupAdmin
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupAdmin`;
CREATE TABLE `IMGroupAdmin` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_menu` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupAdmin
-- ----------------------------
INSERT INTO `IMGroupAdmin` VALUES ('9', 'webapp', '网站管理', '1316267260', '0', '1', '2', '0', 'About');
INSERT INTO `IMGroupAdmin` VALUES ('2', 'System', '系统设置', '1222841259', '0', '1', '20', '0', 'info');
INSERT INTO `IMGroupAdmin` VALUES ('24', 'News', '信息管理', '1375687191', '0', '1', '0', '0', 'News');
INSERT INTO `IMGroupAdmin` VALUES ('29', 'Sysconfig', '核心系统配置', '1437484752', '0', '1', '0', '0', 'info');

-- ----------------------------
-- Table structure for IMGroupClass
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupClass`;
CREATE TABLE `IMGroupClass` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu` char(10) NOT NULL DEFAULT '0',
  `name` char(25) NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `sort` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupClass
-- ----------------------------
INSERT INTO `IMGroupClass` VALUES ('1', 'info', '系统管理', '1', '100');
INSERT INTO `IMGroupClass` VALUES ('20', 'News', '其他管理', '1', '100');
INSERT INTO `IMGroupClass` VALUES ('21', 'About', '网站管理', '1', '100');

-- ----------------------------
-- Table structure for IMGroupClassUser
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupClassUser`;
CREATE TABLE `IMGroupClassUser` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `gc_id` int(6) DEFAULT NULL,
  `uid` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gc` (`gc_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupClassUser
-- ----------------------------
INSERT INTO `IMGroupClassUser` VALUES ('28', '3', '2');
INSERT INTO `IMGroupClassUser` VALUES ('29', '3', '3');
INSERT INTO `IMGroupClassUser` VALUES ('30', '3', '4');
INSERT INTO `IMGroupClassUser` VALUES ('7', '1', '1');
INSERT INTO `IMGroupClassUser` VALUES ('8', '1', '2');
INSERT INTO `IMGroupClassUser` VALUES ('9', '1', '3');
INSERT INTO `IMGroupClassUser` VALUES ('10', '1', '4');
INSERT INTO `IMGroupClassUser` VALUES ('11', '1', '38');
INSERT INTO `IMGroupClassUser` VALUES ('12', '1', '39');
INSERT INTO `IMGroupClassUser` VALUES ('13', '2', '1');
INSERT INTO `IMGroupClassUser` VALUES ('15', '2', '3');
INSERT INTO `IMGroupClassUser` VALUES ('16', '2', '4');
INSERT INTO `IMGroupClassUser` VALUES ('17', '2', '38');
INSERT INTO `IMGroupClassUser` VALUES ('18', '2', '39');
INSERT INTO `IMGroupClassUser` VALUES ('19', '5', '1');
INSERT INTO `IMGroupClassUser` VALUES ('20', '5', '2');
INSERT INTO `IMGroupClassUser` VALUES ('21', '5', '3');
INSERT INTO `IMGroupClassUser` VALUES ('22', '5', '4');
INSERT INTO `IMGroupClassUser` VALUES ('23', '5', '38');
INSERT INTO `IMGroupClassUser` VALUES ('24', '5', '39');
INSERT INTO `IMGroupClassUser` VALUES ('31', '3', '38');

-- ----------------------------
-- Table structure for IMGroupMember
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMember`;
CREATE TABLE `IMGroupMember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '群Id',
  `userId` int(11) unsigned NOT NULL COMMENT '用户id',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否退出群，0-正常，1-已退出',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_userId_status` (`groupId`,`userId`,`status`),
  KEY `idx_userId_status_updated` (`userId`,`status`,`updated`),
  KEY `idx_groupId_updated` (`groupId`,`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和群的关系表';

-- ----------------------------
-- Records of IMGroupMember
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_0
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_0`;
CREATE TABLE `IMGroupMessage_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_0
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_1
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_1`;
CREATE TABLE `IMGroupMessage_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_1
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_2
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_2`;
CREATE TABLE `IMGroupMessage_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_2
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_3
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_3`;
CREATE TABLE `IMGroupMessage_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_3
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_4
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_4`;
CREATE TABLE `IMGroupMessage_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_4
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_5
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_5`;
CREATE TABLE `IMGroupMessage_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_5
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_6
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_6`;
CREATE TABLE `IMGroupMessage_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_6
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroupMessage_7
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_7`;
CREATE TABLE `IMGroupMessage_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_7
-- ----------------------------

-- ----------------------------
-- Table structure for IMGroups
-- ----------------------------
DROP TABLE IF EXISTS `IMGroups`;
CREATE TABLE `IMGroups` (
  `id` mediumint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroups
-- ----------------------------
INSERT INTO `IMGroups` VALUES ('1', '项目组1');
INSERT INTO `IMGroups` VALUES ('2', '项目组2');
INSERT INTO `IMGroups` VALUES ('3', '项目组3');

-- ----------------------------
-- Table structure for IMLog
-- ----------------------------
DROP TABLE IF EXISTS `IMLog`;
CREATE TABLE `IMLog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vc_module` char(50) DEFAULT NULL,
  `vc_operation` char(200) DEFAULT NULL,
  `creator_id` bigint(20) DEFAULT NULL,
  `creator_name` char(50) NOT NULL,
  `vc_ip` char(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `createtime` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=370 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMLog
-- ----------------------------
INSERT INTO `IMLog` VALUES ('193', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357458137');
INSERT INTO `IMLog` VALUES ('350', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438416581');
INSERT INTO `IMLog` VALUES ('351', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438417088');
INSERT INTO `IMLog` VALUES ('352', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438417305');
INSERT INTO `IMLog` VALUES ('353', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438772488');
INSERT INTO `IMLog` VALUES ('354', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438788029');
INSERT INTO `IMLog` VALUES ('355', '系统管理', '用户登录：登录成功！', '1', 'admin', '58.211.28.178', '1', '1439348140');
INSERT INTO `IMLog` VALUES ('356', '系统管理', '用户登录：登录成功！', '1', 'admin', '121.11.169.94', '1', '1439348154');
INSERT INTO `IMLog` VALUES ('357', '系统管理', '用户登录：登录成功！', '1', 'admin', '113.97.189.44', '1', '1439348155');
INSERT INTO `IMLog` VALUES ('358', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.236.53.50', '1', '1439348175');
INSERT INTO `IMLog` VALUES ('359', '系统管理', '用户登录：登录成功！', '1', 'admin', '58.19.78.176', '1', '1439348217');
INSERT INTO `IMLog` VALUES ('360', '系统管理', '用户登录：登录成功！', '1', 'admin', '116.24.248.48', '1', '1439348334');
INSERT INTO `IMLog` VALUES ('361', '系统管理', '用户登录：登录成功！', '1', 'admin', '183.26.224.151', '1', '1439348352');
INSERT INTO `IMLog` VALUES ('362', '系统管理', '用户登录：登录成功！', '1', 'admin', '182.88.163.65', '1', '1439348362');
INSERT INTO `IMLog` VALUES ('363', '系统管理', '用户登录：登录成功！', '1', 'admin', '106.2.213.42', '1', '1439348365');
INSERT INTO `IMLog` VALUES ('364', '系统管理', '用户登录：登录成功！', '1', 'admin', '113.99.103.47', '1', '1439348393');
INSERT INTO `IMLog` VALUES ('365', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439348465');
INSERT INTO `IMLog` VALUES ('366', '系统管理', '用户登录：登录成功！', '1', 'admin', '121.15.10.33', '1', '1439348707');
INSERT INTO `IMLog` VALUES ('367', '系统管理', '用户登录：登录成功！', '1', 'admin', '222.178.223.118', '1', '1439348716');
INSERT INTO `IMLog` VALUES ('368', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.236.53.50', '1', '1439371744');
INSERT INTO `IMLog` VALUES ('369', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439374476');

-- ----------------------------
-- Table structure for IMMessage_0
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_0`;
CREATE TABLE `IMMessage_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_0
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_1
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_1`;
CREATE TABLE `IMMessage_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_1
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_2
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_2`;
CREATE TABLE `IMMessage_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_2
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_3
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_3`;
CREATE TABLE `IMMessage_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_3
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_4
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_4`;
CREATE TABLE `IMMessage_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_4
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_5
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_5`;
CREATE TABLE `IMMessage_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_5
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_6
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_6`;
CREATE TABLE `IMMessage_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_6
-- ----------------------------

-- ----------------------------
-- Table structure for IMMessage_7
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_7`;
CREATE TABLE `IMMessage_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_7
-- ----------------------------

-- ----------------------------
-- Table structure for IMNode
-- ----------------------------
DROP TABLE IF EXISTS `IMNode`;
CREATE TABLE `IMNode` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `title` char(50) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `remark` char(255) DEFAULT NULL,
  `sort` int(6) unsigned DEFAULT NULL,
  `pid` int(6) unsigned NOT NULL,
  `level` int(1) unsigned NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `group_id` int(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=224 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMNode
-- ----------------------------
INSERT INTO `IMNode` VALUES ('50', 'main', '空白首页', '1', '', null, '40', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('84', 'Group', '分组管理', '1', '', '1', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('90', 'Article', '文章管理', '1', '', '100', '1', '2', '0', '9');
INSERT INTO `IMNode` VALUES ('93', 'index', '首页管理', '1', '', '0', '92', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('96', 'index', '列表页', '1', '', '0', '95', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('1', 'Admin', '后台管理', '1', '', '0', '0', '1', '0', '0');
INSERT INTO `IMNode` VALUES ('2', 'Node', '节点管理', '1', '', '2', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('6', 'Role', '角色管理', '1', '', '1', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('7', 'User', '后台用户', '1', '', '3', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('30', 'Public', '公共模块', '1', '', '5', '1', '2', '0', '0');
INSERT INTO `IMNode` VALUES ('31', 'add', '新增', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('32', 'insert', '写入', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('33', 'edit', '编辑', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('39', 'index', '列表', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('37', 'resume', '恢复', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('36', 'forbid', '禁用', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('34', 'update', '更新', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('35', 'foreverdelete', '删除', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('40', 'Index', '默认模块', '1', '', '1', '1', '2', '0', '0');
INSERT INTO `IMNode` VALUES ('49', 'read', '查看', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('125', 'index', '首页模块', '1', null, null, '40', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('127', 'Log', '登录日志管理', '1', '', '8', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('133', 'index', '首页', '1', '', '0', '132', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('152', 'detail', '详情', '1', '', '0', '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('161', 'Bak', '数据库备份', '1', '', '6', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('163', 'GroupClass', '系统导航管理', '1', '', '0', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('168', 'File', '程序文件管理', '1', '', '10', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('204', 'index', '列表', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('208', 'index', '列表', '1', '', '100', '168', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('209', 'add', '新增', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('210', 'edit', '修改', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('211', 'index', '列表', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('207', 'foreverdelete', '删除', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('206', 'edit', '修改', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('205', 'add', '新增', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('212', 'foreverdelete', '删除', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('213', 'foreverdelete', '删除', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('203', 'add', '添加', '1', '', '100', '202', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('214', 'add', '新增', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('215', 'edit', '修改', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('216', 'index', '列表', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('217', 'password', '修改密码', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('219', 'foreverdelete', '删除', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('220', 'add', '新增', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('221', 'edit', '修改', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('222', 'index', '列表', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('223', 'Notice', '全局通知配置', '1', '', '100', '1', '2', '0', '2');

-- ----------------------------
-- Table structure for IMNotice
-- ----------------------------
DROP TABLE IF EXISTS `IMNotice`;
CREATE TABLE `IMNotice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `times` varchar(255) DEFAULT NULL,
  `agentid` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(10) unsigned zerofill DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMNotice
-- ----------------------------
INSERT INTO `IMNotice` VALUES ('6', '这里是第一条全局测试推送通知标题', '这里是第一条全局测试推送通知内容', '1437410982', '999999', null, '0000000001', '0', null);

-- ----------------------------
-- Table structure for IMRecentSession
-- ----------------------------
DROP TABLE IF EXISTS `IMRecentSession`;
CREATE TABLE `IMRecentSession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL COMMENT '用户id',
  `peerId` int(11) unsigned NOT NULL COMMENT '对方id',
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '类型，1-用户,2-群组',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '用户:0-正常, 1-用户A删除,群组:0-正常, 1-被删除',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_userId_peerId_status_updated` (`userId`,`peerId`,`status`,`updated`),
  KEY `idx_userId_peerId_type` (`userId`,`peerId`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRecentSession
-- ----------------------------

-- ----------------------------
-- Table structure for IMRelationShip
-- ----------------------------
DROP TABLE IF EXISTS `IMRelationShip`;
CREATE TABLE `IMRelationShip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smallId` int(11) unsigned NOT NULL COMMENT '用户A的id',
  `bigId` int(11) unsigned NOT NULL COMMENT '用户B的id',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '用户:0-正常, 1-用户A删除,群组:0-正常, 1-被删除',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_smallId_bigId_status_updated` (`smallId`,`bigId`,`status`,`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRelationShip
-- ----------------------------

-- ----------------------------
-- Table structure for IMRole
-- ----------------------------
DROP TABLE IF EXISTS `IMRole`;
CREATE TABLE `IMRole` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `ename` varchar(5) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `ename` (`ename`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRole
-- ----------------------------
INSERT INTO `IMRole` VALUES ('12', '普通客户组', '0', '1', '普通客户组', null, '1438429901', '1438433156');
INSERT INTO `IMRole` VALUES ('13', '系统管理组', '0', '1', '系统管理组', null, '1438429918', '0');

-- ----------------------------
-- Table structure for IMRoleUser
-- ----------------------------
DROP TABLE IF EXISTS `IMRoleUser`;
CREATE TABLE `IMRoleUser` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRoleUser
-- ----------------------------
INSERT INTO `IMRoleUser` VALUES ('4', '27');
INSERT INTO `IMRoleUser` VALUES ('4', '26');
INSERT INTO `IMRoleUser` VALUES ('4', '30');
INSERT INTO `IMRoleUser` VALUES ('5', '31');
INSERT INTO `IMRoleUser` VALUES ('3', '22');
INSERT INTO `IMRoleUser` VALUES ('1', '1');
INSERT INTO `IMRoleUser` VALUES ('1', '2');
INSERT INTO `IMRoleUser` VALUES ('2', '3');
INSERT INTO `IMRoleUser` VALUES ('7', '2');
INSERT INTO `IMRoleUser` VALUES ('3', '35');
INSERT INTO `IMRoleUser` VALUES ('3', '36');
INSERT INTO `IMRoleUser` VALUES ('7', '36');
INSERT INTO `IMRoleUser` VALUES ('3', '37');
INSERT INTO `IMRoleUser` VALUES ('3', '38');
INSERT INTO `IMRoleUser` VALUES ('3', '39');
INSERT INTO `IMRoleUser` VALUES ('1', '4');
INSERT INTO `IMRoleUser` VALUES ('3', '40');
INSERT INTO `IMRoleUser` VALUES ('2', '1');
INSERT INTO `IMRoleUser` VALUES ('2', '41');

-- ----------------------------
-- Table structure for IMUser
-- ----------------------------
DROP TABLE IF EXISTS `IMUser`;
CREATE TABLE `IMUser` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1男2女0未知',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `domain` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '拼音',
  `nick` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '花名,绰号等',
  `password` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(4) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '混淆码',
  `phone` varchar(11) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '手机号码',
  `email` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'email',
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '自定义用户头像',
  `departId` int(11) unsigned NOT NULL COMMENT '所属部门Id',
  `status` tinyint(2) unsigned DEFAULT '0' COMMENT '1. 试用期 2. 正式 3. 离职 4.实习',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `thumb` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '缩略图地址',
  PRIMARY KEY (`id`),
  KEY `idx_domain` (`domain`),
  KEY `idx_name` (`name`),
  KEY `idx_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMUser
-- ----------------------------
INSERT INTO `IMUser` VALUES ('2', '2', 'weixiao', 'weixiao', '微笑哥哥', '6a6784920b5384997ce1f9ac554d9c65', '8020', '13723879918', '512720913@qq.com', './Public/Uploads/20150812/1439382041.jpg', '1', '0', '1439382041', '0', './Public/Uploads/thumb_20150812/1439382041.jpg');
