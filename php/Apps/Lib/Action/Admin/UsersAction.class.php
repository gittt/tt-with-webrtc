<?php

/*
 * 首先感谢使用微笑开发程序，程序中如有不足，请多多指教与指导
 * 使用源代码请勿删除声明文件，辛苦开源贡献代码不容易，请保留作者应该有的知晓权
 * 同时感谢本系统中使用的框架和其他开源作者辛苦劳动成果，感谢！！
 * 微笑交流QQ：512720913 邮箱地址：512720913@qq.com by.weixiao
 */

class UsersAction extends CommonAction {

    public function index() {
        $map = $this->_search('User');

        $this->_list(M('User'), $map);

        parent::index();
    }

    public function getimg() {
        $id = htmlspecialchars($_GET['id']);
        $type = htmlspecialchars($_GET['type']);
        $Users = M('User');
        $str_users = $Users->where(array("id" => $id))->find();

        //获取全部图片和缩略图地址
        $fullimgurl = $str_users['avatar'];
//        
//        $this->fullimgurl = $str_users[];
//        $this->vo = $str_users;
        $this->name = $str_users['name'];
        $this->nick = $str_users['nick'];
        switch ($type) {
            case 1:
                $this->img = $str_users['avatar'];
                break;

            case 2:
                $this->img = $str_users['simg'];
                break;
            case 3:
                $this->img = $str_users['mimg'];
                break;
            default:
                $this->img = $str_users['mimg'];
                break;
        }


      //  $this->vo = $str_users;
        $this->display();
    }

}
