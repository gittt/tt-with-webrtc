<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo (C("sitename")); ?></title>

        <link href="__PUBLIC__/dwz/themes/default/style.css" rel="stylesheet" type="text/css" />
        <link href="__PUBLIC__/dwz/themes/css/core.css" rel="stylesheet" type="text/css" />
        <!--[if IE]>
        <link href="__PUBLIC__/dwz/themes/css/ieHack.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <style type="text/css">
            #header{height:85px}
            #leftside, #container, #splitBar, #splitBarProxy{top:90px}
        </style>
        <script src="__PUBLIC__/dwz/js/speedup.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/jquery.cookie.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/jquery.validate.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/jquery.bgiframe.js" type="text/javascript"></script>
        <script src="__PUBLIC__/xheditor/xheditor-1.2.1.min.js" type="text/javascript"></script>
        <script src="__PUBLIC__/xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/dwz.min.js" type="text/javascript"></script>
        <script src="__PUBLIC__/dwz/js/dwz.regional.zh.js" type="text/javascript"></script>

        <script type="text/javascript">
            function fleshVerify() {
                //重载验证码
                $('#verifyImg').attr("src", '__APP__/Public/verify/' + new Date().getTime());
            }
            function dialogAjaxMenu(json) {
                dialogAjaxDone(json);
                if (json.statusCode == DWZ.statusCode.ok) {
                    //扩展
                    var menuTag = $("#navMenu .selected").attr('menu');
                    $("#sidebar").loadUrl("__GROUP__/Public/menu/menu/" + menuTag);
                }
            }

            function navTabAjaxMenu(json) {
                navTabAjaxDone(json);
                if (json.statusCode == DWZ.statusCode.ok) {
                    //扩展
                    var menuTag = $("#navMenu .selected").attr('menu');
                    $("#sidebar").loadUrl("__GROUP__/Public/menu/menu/" + menuTag);
                }
            }


            function navTabAjaxGroupMenu(json) {
                navTabAjaxDone(json);
                if (json.statusCode == DWZ.statusCode.ok) {
                    //扩展
                    var menuTag = $("#navMenu .selected").attr('menu');
                    $("#sidebar").loadUrl("__GROUP__/Public/menu/menu/" + menuTag);
                }
            }



            $(function () {
                DWZ.init("__PUBLIC__/dwz/dwz.frag.xml", {
                    loginUrl: "__APP__/Public/login_dialog", loginTitle: "登录", // 弹出登录对话框
                    statusCode: {ok: 1, error: 0},
                    pageInfo: {pageNum: "pageNum", numPerPage: "numPerPage", orderField: "_order", orderDirection: "_sort"}, //【可选】
                    debug: false, // 调试模式 【true|false】
                    callback: function () {
                        initEnv();
                        $("#themeList").theme({themeBase: "__PUBLIC__/dwz/themes"});
                    }
                });
            });
        </script>
    </head>

    <body scroll="no">
        <div id="layout">
            <div id="header">
                <div class="headerNav">
                    <a class="logo" href="<?php echo U('Index');?>">Logo</a>
                    <ul class="nav">
                        <li><a href="<?php echo U('Public/main');?>" target="dialog" width="580" height="360" rel="sysInfo">系统消息</a></li>
                        <li><a href="<?php echo U('Public/password');?>" target="dialog" mask="true">修改密码</a></li>
                        <li><a href="<?php echo U('Public/profile');?>" target="dialog" mask="true">修改资料</a></li>
                        <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
                    </ul>
                    <ul class="themeList" id="themeList">
                        <li theme="default"><div class="selected">蓝色</div></li>
                        <li theme="green"><div>绿色</div></li>
                        <li theme="purple"><div>紫色</div></li>
                        <li theme="silver"><div>银色</div></li>
                        <li theme="azure"><div>天蓝</div></li>
                    </ul>
                </div>
                <div id="navMenu">
                    <ul>
                        <?php if(is_array($volist)): $k = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><li <?php if(($k) == "1"): ?>class="selected"<?php endif; ?> menu="<?php echo ($vo["menu"]); ?>"><a href="__GROUP__/Public/menu/menu/<?php echo ($vo["menu"]); ?>" ><span><?php echo ($vo["name"]); ?></span></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                </div>
            </div>

            <div id="leftside">
                <div id="sidebar_s">
                    <div class="collapse">
                        <div class="toggleCollapse"><div></div></div>
                    </div>
                </div>

                <div id="sidebar">
                    	
<div class="accordion" fillSpace="sideBar">
<?php if(is_array($groups)): $i = 0; $__LIST__ = $groups;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$group): $mod = ($i % 2 );++$i;?><div class="accordionHeader">
		<h2><span>Folder</span><?php echo ($group["title"]); ?></h2>
	</div>
	<div class="accordionContent">

		<ul class="tree treeFolder">
			<?php if(is_array($menu[$group['id']])): $i = 0; $__LIST__ = $menu[$group['id']];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i; if(($item['access']) == "1"): if(($item["level"]) == "3"): ?><li>
				<a href="__GROUP__/<?php echo ($parents[$item['pid']]['name']); ?>/<?php echo ($item['name']); ?>" target="navTab" rel="<?php echo ($parents[$item['pid']]['name']); ?>.<?php echo ($item['name']); ?>"><?php echo ($item['title']); ?></a></li>
				<?php else: ?>
				<li><a href="__GROUP__/<?php echo ($item['name']); ?>" target="navTab" rel="<?php echo ($item['name']); ?>"><?php echo ($item['title']); ?></a></li><?php endif; endif; endforeach; endif; else: echo "" ;endif; ?>
		</ul>

	</div><?php endforeach; endif; else: echo "" ;endif; ?>
</div>



                </div>
            </div>

            <div id="container">
                <div id="navTab" class="tabsPage">
                    <div class="tabsPageHeader">
                        <div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
                            <ul class="navTab-tab">
                                <li tabid="main" class="main"><a href="javascript:void(0)"><span><span class="home_icon">我的主页</span></span></a></li>
                            </ul>
                        </div>
                        <div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
                        <div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
                        <div class="tabsMore">more</div>
                    </div>
                    <ul class="tabsMoreList">
                        <li><a href="javascript:void(0)">我的主页</a></li>
                    </ul>
                    <div class="navTab-panel tabsPageContent layoutBox">
                        <div class="page unitBox">
                            <div class="accountInfo">
                                <div class="alertInfo">
                                    <h2>客服QQ:512720913</h2>
                                    <a href="#" target="_blank">使用手册</a>
                                </div>
                                <div class="right">
                                    <p><?php echo (date('Y-m-d g:i a',time())); ?></p>
                                </div>
                                <p><span>尊敬的用户：【<?php echo (session('loginUserName')); ?>】您好，欢迎使用：<?php echo (C("sitename")); ?></span></p>

                                <p>上次登录时间：【<?php echo (date('Y-m-d H:i:s',session('lastLoginTime'))); ?>】 &nbsp;&nbsp;最后登录IP：【<?php echo (session('last_login_ip')); ?>】</p>
                            </div>


                            <div class="pageFormContent" layoutH="80">

                                <p>今日注册用户：<?php echo ($okmoney); ?>  &nbsp; &nbsp; &nbsp;当日活跃人数：<?php echo ($okyw); ?> </p>
                                <div class="divider"></div>
                                <p>总计注册用户：<?php echo ($nomoney); ?>   &nbsp; &nbsp; &nbsp </p>

                                <div class="divider"></div>
                                <h1 style='color:red'><strong><?php echo (C("sitename")); ?>·全局通知信息</strong></h1>

                                <div class="divider"></div>
                                <table class="list" width="100%" layoutH="206">
                                    <thead>
                                        <tr>
                                            <th width="50"><input type="checkbox" group="ids[]" class="checkboxCtrl">编号</th>
                                            <th width="50">消息类型</th>
                                            <th width="100">标题信息</th>

                                            <th width="200">详细内容</th>
                                            <th width="100">发布时间</th>
                                            <th width="60">发布人</th>
                                            <th width="60">状态</th>

                                        </tr>
                                    </thead>
                                    <tbody>


                                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr target="sid_node" rel="<?php echo ($vo['id']); ?>">
                                                <td><input name="ids[]" value="<?php echo ($vo['id']); ?>" type="checkbox">:<?php echo ($vo['id']); ?></td>
                                                <td><?php echo (gettypestatus($vo['type'])); ?></td>
                                                <td><?php echo ($vo['title']); ?></td>
                                                <td><?php echo ($vo['content']); ?></td>
                                                <td><?php echo (date('Y-m-d H:i:s',$vo['times'])); ?></td>
                                                <td><?php echo (get_username($vo['agentid'])); ?></td>
                                                <td><?php echo (getstatus($vo['status'])); ?></td>
                                                <!--					<td> <a href="__URL__/foreverdelete/id/<?php echo ($vo["id"]); ?>/navTabId/__MODULE__" target="navTabTodo" title="你确定要删除吗？">删除</a></td>-->
                                                <!--<td> <a href="#" target="navTabTodo" title="你确定要快捷办理业务吗？">快捷办理业务</a></td>-->

                                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>


                                    </tbody>
                                </table>
                                <div class="panelBar">
                                    <div class="pages">
                                        <span>显示</span>
                                        <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage: this.value})">
                                            <option value="5" <?php if(($numPerPage) == "5"): ?>selected=selected<?php endif; ?>>5</option>
                                            <option value="10" <?php if(($numPerPage) == "10"): ?>selected=selected<?php endif; ?>>10</option>
                                            <option value="15" <?php if(($numPerPage) == "15"): ?>selected=selected<?php endif; ?>>15</option>
                                            <option value="20" <?php if(($numPerPage) == "20"): ?>selected=selected<?php endif; ?>>20</option>
                                        </select>
                                        <span>共<?php echo ($totalCount); ?>条</span>

                                    </div>
                                    <div class="pagination" targetType="navTab" totalCount="<?php echo ($totalCount); ?>" numPerPage="<?php echo ($numPerPage); ?>" pageNumShown="10" currentPage="<?php echo ($currentPage); ?>"></div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>

        <div id="footer">Copyright &copy; 2014 - 2019 <a href="<?php echo (C("SITEURL")); ?>" target="_blank"><?php echo (C("sitename")); ?></a></div>


    </body>
</html>