<?php
return array(
	'SHOW_RUN_TIME'=>FALSE,			// 运行时间显示
	'SHOW_ADV_TIME'=>FALSE,			// 显示详细的运行时间
	'SHOW_DB_TIMES'=>FALSE,			// 显示数据库查询和写入次数
	'SHOW_CACHE_TIMES'=>FALSE,		// 显示缓存操作次数
	'SHOW_USE_MEM'=>FALSE,			// 显示内存开销
	'SHOW_PAGE_TRACE'=>FALSE			//显示调试信息
);
?>